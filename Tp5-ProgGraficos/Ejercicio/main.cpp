#include <iostream>
using namespace std;
//Include nana/gui.hpp header file to enable Nana C++ Library
//for the program.
#include <nana/gui.hpp>
//Include a label widget, we will use it in this example.
#include <nana/gui/widgets/label.hpp>
int main()
{
	using namespace nana;
	form fm;
	label lb{ fm, rectangle{ 10, 10, 100, 100 } };
	lb.caption("     Scores");
	fm.show();
	exec();
}